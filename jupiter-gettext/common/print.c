#include <confguard.h>

#include "jupcommon.h"

#include <stdio.h>
#include <stdlib.h>

#if HAVE_PTHREAD
# include <pthread.h>
#endif

static void *
print_it (void *data)
{
	const char **strings = (const char **) data;
	printf (_("%s from %s!\n"), strings[0], strings[1]);
	return 0;
}

int
print_routine (const char *salutation, const char *name)
{
	/* might seg fault on some machines */
	/* const char **strings = { salutation , name }; */
	const char *strings[] = { salutation , name };
#if ASYNC_EXEC
	pthread_t tid;
	pthread_create(&tid, 0, print_it, (void *)strings);
	pthread_join(tid, 0);
#else
	print_it(strings);
#endif
	return 0;
}

