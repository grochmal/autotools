# SOME DESCRIPTIVE TITLE.
# This file is put in the public domain.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: jupiter 0.1\n"
"Report-Msgid-Bugs-To: jupiter@example.com\n"
"POT-Creation-Date: 2014-12-01 23:19+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: common/print.c:16
#, c-format
msgid "%s from %s!\n"
msgstr ""

#: src/main.c:18
msgid "Hello"
msgstr ""

#: src/main.c:62
#, c-format
msgid "Using locale `%s', directory `%s', domain `%s'\n"
msgstr ""

#: src/main.c:69
#, c-format
msgid "trying to load module `%s'\n"
msgstr ""

#: src/modules/hithere/hithere.c:7
msgid "Hi there"
msgstr ""

#: src/modules/goodbye/goodbye.c:7
msgid "Goodbye"
msgstr ""
