#ifndef __MODULE_H
#define __MODULE_H

#define GET_SALUTATION_SYM "get_salutation"

typedef const char * get_salutation_t (void);
const char *
get_salutation (void);

#endif  /* __MODULE_H */

