#include <libjupiter.h>
#include <confguard.h>
#include "module.h"

#if HAVE_STRING_H
# include <string.h>
#elif HAVE_STRINGS_H
# include <strings.h>
#elif HAVE_MEMORY_H
# include <memory.h>
#endif

#include <stdio.h>
#include <stdlib.h>

#include <ltdl.h>

#define DEFAULT_SALUTATION "Hello"

char *
module_name ( const char *prefix, const char *name
	    , const char *suffix, const char *deflt )
{
	size_t size;
	char *mod_name;
	const char *mod;

	if (NULL == name)
		mod = deflt;
	else
		mod = name;
	size = strlen(prefix) + strlen(mod) * 2 + 1 + strlen(suffix) + 1;
	mod_name = (char *) malloc(sizeof(char)*size);
	if (NULL != module_name) {
		strcpy(mod_name, prefix);
		strcat(mod_name, mod);
		strcat(mod_name, "/");
		strcat(mod_name, mod);
		strcat(mod_name, suffix);
		return mod_name;
	}
	return NULL;
}

int
main (int argc, char **argv)
{
	const char *salutation = DEFAULT_SALUTATION;
	char *mod_name = NULL;
	int ltdl;
	lt_dlhandle module;
	get_salutation_t *get_salutation_fp = 0;

	LTDL_SET_PRELOADED_SYMBOLS();

	mod_name = module_name("modules/", argv[1], ".la", "hithere");
	ltdl = lt_dlinit();
	if (0 == ltdl) {
		printf("trying to load module `%s'\n", mod_name);
		module = lt_dlopen(mod_name);
		if (0 != module) {
			get_salutation_fp = (get_salutation_t *)
				lt_dlsym(module, GET_SALUTATION_SYM);
			if (0 != get_salutation_fp)
				salutation = get_salutation_fp();
		}
	}
	free(mod_name);
	jupiter_print(salutation, argv[0]);
	if (0 == ltdl) {
		if (0 != module)
			lt_dlclose(module);
		lt_dlexit();
	}
	return 0;
}

