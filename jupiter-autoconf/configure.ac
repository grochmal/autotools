#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

AC_PREREQ([2.63])
AC_INIT([jupiter], [0.1], [jupiter-bugs@orion.org])
AC_CONFIG_SRCDIR([src/main.c])
AC_CONFIG_HEADERS([config.h])

# Checks for programs.
AC_PROG_CC
AC_PROG_CPP
AC_PROG_INSTALL

# Check for command-line options
AC_ARG_ENABLE([async-exec],
              [AS_HELP_STRING([--disable-async-exec],
                [disable async execution feature @<:@default: no@:>@])],
              [async_exec=${enableval}],
              [async_exec=yes])

# Checks for libraries.
have_pthread=no
if test xyes = "x${async_exec}"; then
  AC_SEARCH_LIBS([pthread_create], [pthread], [have_pthread=yes])

  if test xyes = "x${have_pthread}"; then
    AC_CHECK_HEADERS([pthread.h], [], [have_pthread=no])
  fi

  if test xno = "x${have_pthread}"; then
    AC_WARN([
    ---------------------------------------
    Unable to find pthreads on this system.
    Building a single threaded version.
    ---------------------------------------])
    async_exec=no
  fi
fi

if test xyes = "x${async_exec}"; then
  AC_DEFINE([ASYNC_EXEC], 1, [async execution enabled])
fi

# Checks for header files.
AC_CHECK_HEADERS([stdlib.h])

# Checks for typedefs, structures, and compiler characteristics.
AC_TYPE_UINT32_T

# Checks for library functions.

AC_CONFIG_FILES([Makefile
                 src/Makefile])
AC_OUTPUT

echo \
"-----------------------------------------------------------

${PACKAGE_NAME} Version ${PACKAGE_VERSION}

Prefix: '${prefix}'
Compiler: '${CC} ${CFLAGS} ${CPPFLAGS}'

Package features:
  Async execution: ${async_exec}

Libraries used:
  pthread: ${have_pthread}

Now type make @<:@target@:>@
  where the optional <target> is:
    all      - build all binaries
    install  - install everything

------------------------------------------------------------"

