# Brazilian portuguese messages for jupiter.
# This file is put in the public domain.
# Michal Grochmal <grochmal at member dot fsf dot org>, 2014.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: jupiter 0.1\n"
"Report-Msgid-Bugs-To: jupiter@example.com\n"
"POT-Creation-Date: 2014-12-01 23:19+0000\n"
"PO-Revision-Date: 2014-12-01 23:33+0\n"
"Last-Translator: Michal Grochmal <grochmal at member dot fsf dot org>\n"
"Language-Team: Michal Grochmal <grochmal at member dot fsf dot org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: common/print.c:16
#, c-format
msgid "%s from %s!\n"
msgstr "%s, fala %s!\n"

#: src/main.c:18
msgid "Hello"
msgstr "Ola"

#: src/main.c:62
#, c-format
msgid "Using locale `%s', directory `%s', domain `%s'\n"
msgstr "Uso o locale `%s', diretório `%s', domínio `%s'\n"

#: src/main.c:69
#, c-format
msgid "trying to load module `%s'\n"
msgstr "tento carregar o módulo `%s'\n"

#: src/modules/hithere/hithere.c:7
msgid "Hi there"
msgstr "Oi"

#: src/modules/goodbye/goodbye.c:7
msgid "Goodbye"
msgstr "Tchau"
